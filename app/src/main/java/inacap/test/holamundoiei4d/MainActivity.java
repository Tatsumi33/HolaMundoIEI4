package inacap.test.holamundoiei4d;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import inacap.test.holamundoiei4d.vista.FormularioActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextUsername, editPassword;
    private Button btLogin;
    private TextView tvUsername, tvRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.editTextUsername = (EditText) findViewById(R.id.etUsername);
        this.editPassword = (EditText) findViewById(R.id.etPassword);

        this.btLogin = (Button) findViewById(R.id.btLogin);
        this.tvUsername = (TextView) findViewById(R.id.tvUsername);
        this.tvRegistrar = (TextView) findViewById(R.id.tvRegistrar);

        this.btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener el nombre de usuario
                String nombre_usuario = editTextUsername.getText().toString();

                // Mostrar el contenido en TextView
                tvUsername.setText(nombre_usuario);

                // Mostrar en Toast (mensaje temporal)
                Toast.makeText(getApplicationContext(), "Username: " + nombre_usuario, Toast.LENGTH_SHORT).show();

            }
        });

        this.tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Iniciar la segunda ventana
                Intent nuevaVentana = new Intent(MainActivity.this, FormularioActivity.class);
                startActivity(nuevaVentana);
            }
        });

    }
}











